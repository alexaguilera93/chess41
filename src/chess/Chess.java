package chess;
import java.util.Scanner;
import ChessBoard.ChessBoard;

/**
 * This class is taking user input from the terminal and validating it.
 * It check that the user input is valid by the criteria specified in the project
 * documentation and it will allow the move to be processed into the chessboard API.
 * 
 * @author Alex Aguilera
 * @version 1.1
 */

public final class Chess {
	//boolean to end main game loop
	static boolean gameOver;

	//for easier comparisons when needed
	public static final String w = "White";
	public static final String b = "Black";

	/**
	 * Main Method. Program Starts Here.
	 *
	 * @param args
     */
	public static void main(String[] args) {
		
		gameOver = false;
		String whosTurn = w;
		String move = "";
		CheckInput check = new CheckInput();
		boolean proposedDraw = false;
		ChessBoard board = ChessBoard.getInstance();
		
		System.out.println(board + "\n");
		while(!gameOver){
			
			Scanner user_input = new Scanner(System.in);
			System.out.print(whosTurn + "'s Move: ");
			move = user_input.nextLine();
			
			//Check if text is at least valid
			if(!check.validTypedMove(move)){
				System.out.println("Illegal Move, try again");
				continue;
			}
			
			//see if a player resigned
			if(check.resigned(move)){
				if(whosTurn.equals(w)){
					System.out.println(b + " wins");
				}else{
					System.out.println(w + " wins");
				}
				gameOver = true;
				continue;
			}
			
			//if there was a draw proposed, check the input and possibly end the game
			if(proposedDraw){
				if(move.equals("draw")){
					System.out.println("draw");
					gameOver = true;
					continue;
				}else{
					proposedDraw = false;
				}
			}
			
			//see if the player proposed a draw at the end of input
			if(check.proposeDraw(move)){
				proposedDraw = true;
			}
			
			
			
			//
			//code to execute move and redraw the board should go here
			//
			String start = move.substring(0,2);
			String finish = move.substring(3,5);
			if(finish.charAt(1) == '8' || finish.charAt(1) == '1'){
				if(move.length() == 7){
					if(move.charAt(6) == 'R' || move.charAt(6) == 'N' || move.charAt(6) == 'B'){
						board.setPromo(move.charAt(6));
					}
				}else{
					board.setPromo('Q');
				}
			}
			
			char color = whosTurn.equals(w) ? 'w' : 'b';
			String result = board.movePiece(start, finish, color);
			if(result.equals("true")){
				System.out.println("\n" + board + "\n");
				//System.out.println(result);
				whosTurn = whosTurn.equals(w) ? b : w;
				continue;
			}else if(result.length() > 9 && result.substring(0,9).equals("Checkmate")){
				System.out.println("\n" + board + "\n");
				System.out.println("Checkmate");
				String winner = whosTurn;
				System.out.println(winner + " wins");
				gameOver=true;
				continue;
			}else if(result.length() == 5 && result.equals("Check")){
				System.out.println("\n" + board + "\n");
				System.out.println(result);
				whosTurn = whosTurn.equals(w) ? b : w;
				continue;
			}else if(result.equals("false")){
				System.out.println("Illegal move, try again");
				continue;
			}else if(result.equals("Stalemate")){
				System.out.println("Stalemate");
				gameOver=true;
				continue;
			}
			

		}
		
	}

}
