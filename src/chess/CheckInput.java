package chess;
/**
 * TODO Provide some documentation.
 * I have no idea what you are doing with this class or how to use it if I need to.
 */

/**
 * Class to check user input for errors/typos
 *
 * @author Alex Aguilera
 * @version 1.0
 */
final class CheckInput {
	/**
	 * constructor
	 */
	public CheckInput(){
		
	}
	
	/**
	 * @param moveText Text a user input
	 * @return True if user typed a theoretically valid move, false otherwise
	 */
	public boolean validTypedMove(String moveText){
		if(moveText.equals("resign") || moveText.equals("draw") || moveText.equals("draw?")){
			return true;
		}
		if(moveText.length() < 5){
			return false;
		}
		
		char firstLetter = moveText.charAt(0);
		char secondLetter = moveText.charAt(3);
		char firstNum = moveText.charAt(1);
		char secondNum = moveText.charAt(4);

        /**
         * Checking to see if the first and second letter are a-h
         * and first and second numbers are 1-8.
         * 
         */
		if(firstLetter < 'a' ||  firstLetter > 'h'){
			return false;
		}
		if(secondLetter < 'a' || secondLetter > 'h'){
			return false;
		}
        if(firstNum < '1' || firstNum > '8'){
            return false;
        }
        if(secondNum < '1' || secondNum > '8'){
            return false;
        }
        if(moveText.length() == 5){
        	return true;
        }
		return true;
	}
	
	/**
	 *@return true if user typed resign, false otherwise
	 *@param moveText user input text for move
	 */
	public boolean resigned(String moveText){
		return moveText.equals("resign") ? true : false;
	}
	
	/**
	 *@return true if user typed draw?, false otherwise
	 *@param moveText user input text for move
	 */
	public boolean proposeDraw(String moveText){
		if(moveText.length() >5){
			//System.out.println(moveText.substring(moveText.length() - 5, moveText.length()));
			if(moveText.substring(moveText.length() - 5, moveText.length()).equals("draw?")){
				return true;
			}else{
				return false;
			}
		}
		return false;
	}
}
