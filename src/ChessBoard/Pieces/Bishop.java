package ChessBoard.Pieces;

import java.util.HashMap;
import java.util.Map;

/**
 * Class Defines a Bishop
 *
 * @author Jaya Kasa
 * @version 1.1
 */
final class Bishop extends Piece{

    //use to calculate all future moves
    private Map<String, int[][]> offsets = null;
    private final int[][] xy = {{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}, {7, 7}};
    private final int[][] xym = {{1, -1}, {2, -2}, {3, -3}, {4, -4}, {5, -5}, {6, -6}, {7, -7}};
    private final int[][] xmy = {{-1, 1}, {-2, 2}, {-3, 3}, {-4, 4}, {-5, 5}, {-6, 6}, {-7, 7}};
    private final int[][] xmym = {{-1, -1}, {-2, -2}, {-3, -3}, {-4, -4}, {-5, -5}, {-6, -6}, {-7, -7}};

    Bishop(){
        offsets = new HashMap<>();
        offsets.put("xy", xy);
        offsets.put("-xy", xmy);
        offsets.put("x-y", xym);
        offsets.put("-x-y", xmym);
    }
    @Override
    public Map<String, int[][]> getOffset() {
        return offsets;
    }
}
