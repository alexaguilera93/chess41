package ChessBoard.Pieces;

import java.util.HashMap;
import java.util.Map;

/**
 * Class Defines a Knight
 *
 * @author Jaya Kasa
 * @version 1.0
 */
final class Knight extends Piece{

    //use to calculate all future moves
    private Map<String, int[][]> offsets = null;

    private final int[][] xy = {{1, 2}, {2, 1}};
    private final int[][] xym = {{1, -2}, {2, -1}};
    private final int[][] xmy = {{-1, 2}, {-2, 1}};
    private final int[][] xmym = {{-1, -2}, {-2, -1}};

    Knight(){
        offsets = new HashMap<>();
        offsets.put("xy", xy);
        offsets.put("-xy", xmy);
        offsets.put("x-y", xym);
        offsets.put("-x-y", xmym);
    }
    @Override
    public Map<String, int[][]> getOffset() {
        return offsets;
    }
}
