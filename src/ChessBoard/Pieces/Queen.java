package ChessBoard.Pieces;

import java.util.HashMap;
import java.util.Map;

/**
 * Class Defines a Queen
 *
 * @author Jaya Kasa
 * @version 1.0
 */
final class Queen extends Piece{

    //use to calculate all future moves
    private Map<String, int[][]> offsets = null;

    private final int[][] y = {{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}};
    private final int[][] ym = {{0, -1}, {0, -2}, {0, -3}, {0, -4}, {0, -5}, {0, -6}, {0, -7}};
    private final int[][] x = {{1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}};
    private final int[][] xm = {{-1, 0}, {-2, 0}, {-3, 0}, {-4, 0}, {-5, 0}, {-6, 0}, {-7, 0}};

    private final int[][] xy = {{1, 1}, {2, 2}, {3, 3}, {4, 4}, {5, 5}, {6, 6}, {7, 7}};
    private final int[][] xym = {{1, -1}, {2, -2}, {3, -3}, {4, -4}, {5, -5}, {6, -6}, {7, -7}};
    private final int[][] xmy = {{-1, 1}, {-2, 2}, {-3, 3}, {-4, 4}, {-5, 5}, {-6, 6}, {-7, 7}};
    private final int[][] xmym = {{-1, -1}, {-2, -2}, {-3, -3}, {-4, -4}, {-5, -5}, {-6, -6}, {-7, -7}};

    Queen(){
        offsets = new HashMap<>();
        offsets.put("x", x);
        offsets.put("-x", xm);
        offsets.put("y", y);
        offsets.put("-y", ym);
        offsets.put("xy", xy);
        offsets.put("-xy", xmy);
        offsets.put("x-y", xym);
        offsets.put("-x-y", xmym);
    }
    @Override
    public Map<String, int[][]> getOffset() {
        return offsets;
    }
}
