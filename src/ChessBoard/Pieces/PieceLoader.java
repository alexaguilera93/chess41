package ChessBoard.Pieces;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Class to load specific pieces.
 *
 * @author Jaya Kasa
 * @version 1.0
 */
public final class PieceLoader {

    //private constructor to prevent initialization
    private PieceLoader(){
    }

    /**
     * Class to load Pieces
     *
     * @return Map containing chess pieces
     */
    public static Map<String, Piece> loadedPieces(){
        Map<String, Piece> pieces = new HashMap<>();

        //loading pieces
        pieces.put("p", new Pawn());
        pieces.put("K", new King());
        pieces.put("Q", new Queen());
        pieces.put("R", new Rook());
        pieces.put("B", new Bishop());
        pieces.put("N", new Knight());

        return pieces;
    }
}
