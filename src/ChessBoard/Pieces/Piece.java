package ChessBoard.Pieces;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Parent class to all chess pieces
 *
 * @author Jaya Kasa
 * @version 1.6
 */
public abstract class  Piece {

    public static final Set<String> whitePromotion = new HashSet<>();
    public static final Set<String> blackPromotion = new HashSet<>();
    public static final String origBlackBishop = "f8";
    public static final String origBlackKnight = "g8";
    public static final String origBlackRook = "h8";
    public static final String origWhiteBishop = "f1";
    public static final String origWhiteKnight = "g1";
    public static final String origWhiteRook = "h1";


    {
        //white promo locations
        whitePromotion.add("8a");
        whitePromotion.add("8b");
        whitePromotion.add("8c");
        whitePromotion.add("8d");
        whitePromotion.add("8e");
        whitePromotion.add("8f");
        whitePromotion.add("8g");
        whitePromotion.add("8h");

        //black promo locations
        blackPromotion.add("1a");
        blackPromotion.add("1b");
        blackPromotion.add("1c");
        blackPromotion.add("1d");
        blackPromotion.add("1e");
        blackPromotion.add("1f");
        blackPromotion.add("1g");
        blackPromotion.add("1h");
    }

    /**
     * Method for getting the specific offsets allowed for a chess piece
     *
     * @return
     */
    public abstract Map<String, int[][]> getOffset();
}
