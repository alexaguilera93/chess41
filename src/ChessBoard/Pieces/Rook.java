package ChessBoard.Pieces;

import java.util.HashMap;
import java.util.Map;

/**
 * Class Defines a Rook
 *
 * @author Jaya Kasa
 * @version 1.0
 */
final class Rook extends Piece{

    //use to calculate all future moves
    private Map<String, int[][]> offsets = null;
    private final int[][] y = {{0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, {0, 6}, {0, 7}};
    private final int[][] ym = {{0, -1}, {0, -2}, {0, -3}, {0, -4}, {0, -5}, {0, -6}, {0, -7}};
    private final int[][] x = {{1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}};
    private final int[][] xm = {{-1, 0}, {-2, 0}, {-3, 0}, {-4, 0}, {-5, 0}, {-6, 0}, {-7, 0}};

    Rook(){
        offsets = new HashMap<>();
        offsets.put("x", x);
        offsets.put("-x", xm);
        offsets.put("y", y);
        offsets.put("-y", ym);
    }

    @Override
    public Map<String, int[][]> getOffset() {
        return offsets;
    }
}
