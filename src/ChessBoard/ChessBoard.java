package ChessBoard;

import ChessBoard.Board.Board;

/**
 * Static Class for Building the chessboard
 *
 * @author Jaya Kasa
 * @version 1.0
 */
public final class ChessBoard {

    //instance vars
    private Board board;

    //class vars
    private static ChessBoard chessBoard = null;

    /**
     * Private Constructor to prevent initialization.
     */
    private ChessBoard(){
        board = Board.getInstance();
        //legal = Legal.getInstance();
    }

    /**
     *
     * Returns an instance of the ChessBoard.
     * Only one instance will ever exist.
     *
     * @return ChessBoard
     */
    public static ChessBoard getInstance(){
        if(chessBoard == null){
            chessBoard = new ChessBoard();
        }
        return chessBoard;
    }

    /**
     * Method to move a chess piece on the board.
     *
     * @param start
     * @param finish
     * @return
     */
    public String movePiece(String start, String finish, char color){
        //reverse the incoming coordinates
        start = start.charAt(1) + "" + start.charAt(0);
        finish = finish.charAt(1) + "" + finish.charAt(0);

        //System.out.println("ChessBoard " + start + " " + finish);

        return board.setPiece(start, finish, color);

        
    }

    /**
     * Method to promote a piece
     *
     * @param a
     */
    public void setPromo(char a){
    	board.setPromo(a);
    }

    @Override
    public String toString() {
        return board.toString();
    }
}
