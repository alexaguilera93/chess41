package ChessBoard.Board;

import ChessBoard.Pieces.Piece;
import ChessBoard.Pieces.PieceLoader;

import java.util.*;

/**
 * Class Checks the legality of certain moves, loads future moves.
 *
 * @author Jaya Kasa
 * @version 1.0
 */
public final class Legal {

    private static Legal legal = null;
    private MoveStore black;
    private MoveStore white;

    private String blackKing = "";
    private String whiteKing = "";
    private final Map<String, Piece> pieces = PieceLoader.loadedPieces();
    private Map<String, Boolean> enpassantBlack;
    private Map<String, Boolean> enpassantWhite;
    private boolean pawn;
    private boolean cross;
    private boolean forward;
    private boolean enemy;
    private Map<String, String> enpassant = null;

    private boolean[] occupiedBlack = {true, true, true, true};
    private boolean[] occupiedWhite = {true, true, true, true};

    //check/checkmate for black
    private boolean checkB;
    private boolean checkMateB;

    //check/checkmate for white
    private boolean checkW;
    private boolean checkMateW;

    /**
     * Private constructor to initialize the Legal object
     */
    private Legal(){
        black = MoveStore.getBlackMoveStore();
        white = MoveStore.getWhiteMoveStore();
        checkB = false;
        checkMateB = false;
        checkW = false;
        checkMateW = false;

        //setup enpassant
        enpassantBlack = new HashMap<>();
        enpassantWhite = new HashMap<>();
        loadEnPassant();
        enpassant = new HashMap<>();

        pawn = false;
        cross = false;
        enemy = false;
        forward = false;
    }

    /**
     * Static Method to ensure that only one instance of a specific piece exists
     *
     * @return single Legal instance
     */
    static Legal getInstance(){
        if(legal == null){
            legal = new Legal();
        }
        return legal;
    }

    /**
     * Method checks if a givel move is legal for a given color
     *
     * @param color
     * @param start
     * @param finish
     * @return true if legal, else false
     */
    boolean isLegal(char color, String start, String finish){
        ////System.out.println("Legal isLegal: " + color + " " + start + " " + finish);
        checkB = checkW = false;

        boolean allowed = false;
        if(color == 'b'){
            allowed = black.hasMove(start,finish);
            ////System.out.println("Black " + start + " allowed " + allowed);

            //setup enpassant
            if(allowed){
                String piece = Board.getPiece(start);

                if(start.compareTo(Piece.origBlackBishop) == 0){
                    setCastle(1,'b');
                }
                if(start.compareTo(Piece.origBlackKnight) == 0){
                    setCastle(2,'b');
                }

                if(enpassantBlack.containsKey(finish)){
                    if(piece.compareTo("bp") == 0) {
                        ////System.out.println("Does it exist? " + enpassantBlack.containsKey(finish));
                        enpassantBlack.remove(finish);
                        ////System.out.println("Did it remove? " + !enpassantBlack.containsKey(finish));
                        enpassantBlack.put(finish, true);
                        ////System.out.println("Is it set? " + enpassantBlack.get(finish));
                    }
                }
                //turnoff enpassant
                if(enpassantBlack.containsKey(start)){
                    if(enpassantBlack.get(start)){
                        if(piece.compareTo("bp") == 0) {
                            enpassantBlack.remove(start);
                        }
                    }
                }

                /*dealing with promotion
                if(Piece.blackPromotion.contains(finish)){
                    //implement the promotion here
                    Scanner scanner = new Scanner(//System.in);
                    //System.out.println("Promoting piece. What would you like to set piece as?");
                    while(true){
                        //System.out.println("Your choices are: p, Q, R, N, B. Case-sensitive");
                        piece = scanner.nextLine();
                        if(piece.compareTo("K") == 0 || !pieces.containsKey(piece)){
                            //System.out.println("Illegal Input.");
                            continue;
                        }
                        else if(piece.compareTo("p") == 0){
                            break;
                        }
                        else if(pieces.containsKey(piece)){
                            piece = "b" + piece;
                            Board.promotePiece(finish, piece);
                            break;
                        }
                    }
                }*/
            }
        }
        else{
            //white.printMoves("White " + start);
            allowed = white.hasMove(start,finish);

            //setup enpassant
            if(allowed){

                if(start.compareTo(Piece.origWhiteBishop) == 0){
                    setCastle(1,'b');
                }
                if(start.compareTo(Piece.origWhiteKnight) == 0){
                    setCastle(2,'b');
                }

                String piece = Board.getPiece(start);
                if(enpassantWhite.containsKey(finish)){
                    if(piece.compareTo("wp") == 0) {
                        ////System.out.println("Does it exist? " + enpassantWhite.containsKey(finish));
                        enpassantWhite.remove(finish);
                        ////System.out.println("Did it remove? " + !enpassantWhite.containsKey(finish));
                        enpassantWhite.put(finish, true);
                        ////System.out.println("Is it set? " + enpassantWhite.get(finish));
                    }
                }
                //turnoff enpassant
                if(enpassantWhite.containsKey(start)){
                    if(enpassantWhite.get(start)) {
                        if (piece.compareTo("wp") == 0) {
                            enpassantWhite.remove(start);
                        }
                    }
                }

                /*dealing with promotion
                if(Piece.whitePromotion.contains(finish)){
                    //implement the promotion here
                    Scanner scanner = new Scanner(//System.in);
                    //System.out.println("Promoting piece. What would you like to set piece as?");
                    while(true){
                        //System.out.println("Your choices are: p, Q, R, N, B. Case-sensitive");
                        piece = scanner.nextLine();

                        if(piece.compareTo("K") == 0 || !pieces.containsKey(piece)){
                            //System.out.println("Illegal Input.");
                            continue;
                        }
                        if(piece.compareTo("p") == 0){
                            break;
                        }
                        if(pieces.containsKey(piece)){
                            piece = "w" + piece;
                            Board.promotePiece(finish, piece);
                            break;
                        }
                    }
                }*/
            }
        }

        return allowed;
    }

    /**
     * Method loads the MoveStore and sets it for white pieces.
     *
     * @param location
     */
    void setupMovesWhite(String location){
        white.remove(location);
        Set<String> moves = new HashSet<>();

        String piece = Board.getBoard().get(location).charAt(1) + "";

        ////System.out.println("Current location: " + location + " Piece: w " + piece);

        // saving the location of the king for future reference
        if(piece.compareTo("K") == 0){
            blackKing = location;
            //handling castling
            if(occupiedWhite[0] && !occupiedWhite[1] && !occupiedWhite[2] && occupiedWhite[3]){
                moves.add(Piece.origWhiteRook);
                occupiedWhite[0] = occupiedWhite[3] = false;
            }
        }
        if(piece.compareTo("p") == 0)
            pawn = true;

        ////System.out.println("King location: " + blackKing);
        ////System.out.println("Pawn: " + pawn);

        if(!pawn && pieces.get(piece).getOffset().containsKey("x")){
            enemy = false;
            int[][] x = pieces.get(piece).getOffset().get("x");
            exploreW(x, location, 0, moves);
        }
        if (pieces.get(piece).getOffset().containsKey("y")){
            //if(pawn)
            //    //System.out.println("y white explore");
            enemy = false;
            forward = true;
            int[][] y = pieces.get(piece).getOffset().get("y");
            exploreW(y, location, 0, moves);
            forward = false;
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("-x")){
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("-x");
            exploreW(xm, location, 0, moves);
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("-y")){
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("-y");
            exploreW(xm, location, 0, moves);
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("x-y")){
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("x-y");
            cross = true;
            exploreW(xm, location, 0, moves);
            cross = false;
        }
        if (pieces.get(piece).getOffset().containsKey("-xy")){
            //if(pawn)
            //    //System.out.println("-xy white explore");
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("-xy");
            cross = true;
            exploreW(xm, location, 0, moves);
            cross = false;
        }
        if (pieces.get(piece).getOffset().containsKey("xy")){
            //if(pawn)
            //    //System.out.println("xy white explore");
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("xy");
            cross = true;
            exploreW(xm, location, 0, moves);
            cross = false;
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("-x-y")){
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("-x-y");
            cross = true;
            exploreW(xm, location, 0, moves);
            cross = false;
        }
        //going to load the moves for enpassant
        if(pawn){
            String enp = loadEnPassantLocations(location, false);
            ////System.out.println("ENP IS " + enp);
            if(enpassantWhite.containsKey(enp)){
                moves.add(enp);
            }
        }

        //implement way to remove moves from moves set based on special rules
        //ex: pawns, castling, check, checkmate


        //added the new moves to the movestore
        white.setMoves(location, moves);
        pawn = false;

        /*
        //System.out.print("Setting: ");
        for (String loc:moves) {
            //System.out.print(loc + " ");
        }
        //System.out.println();
        */
    }

    /**
     * Method loads the MoveStore and sets it for black pieces.
     *
     * @param location
     */
    void setupMovesBlack(String location){
        black.remove(location);
        Set<String> moves = new HashSet<>();

        String piece = Board.getBoard().get(location).charAt(1) + "";

        ////System.out.println("Current location: " + location + " Piece: w " + piece);

        // saving the location of the king for future reference
        if(piece.compareTo("K") == 0){
            blackKing = location;
            //handling castling
            if(occupiedWhite[0] && !occupiedWhite[1] && !occupiedWhite[2] && occupiedWhite[3]){
                moves.add(Piece.origBlackRook);
                occupiedBlack[0] = occupiedBlack[3] = false;
            }
        }
        if(piece.compareTo("p") == 0)
            pawn = true;

        ////System.out.println("King location: " + blackKing);
        ////System.out.println("Pawn: " + pawn);

        if(!pawn && pieces.get(piece).getOffset().containsKey("x")){
            enemy = false;
            int[][] x = pieces.get(piece).getOffset().get("x");
            exploreB(x, location, 0, moves);
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("y")){
            enemy = false;
            int[][] y = pieces.get(piece).getOffset().get("y");
            exploreB(y, location, 0, moves);
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("-x")){
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("-x");
            exploreB(xm, location, 0, moves);
        }
        if (pieces.get(piece).getOffset().containsKey("-y")){
            if(pawn)
                ////System.out.println("-y black explore");
            enemy = false;
            forward = true;
            int[][] xm = pieces.get(piece).getOffset().get("-y");
            exploreB(xm, location, 0, moves);
            forward = false;
        }
        if (pieces.get(piece).getOffset().containsKey("x-y")){
            //if(pawn)
            //    //System.out.println("x-y black explore");
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("x-y");
            cross = true;
            exploreB(xm, location, 0, moves);
            cross = false;
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("-xy")){
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("-xy");
            cross = true;
            exploreB(xm, location, 0, moves);
            cross = false;
        }
        if (!pawn && pieces.get(piece).getOffset().containsKey("xy")){
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("xy");
            cross = true;
            exploreB(xm, location, 0, moves);
            cross = false;
        }
        if (pieces.get(piece).getOffset().containsKey("-x-y")){
            //if(pawn)
            //    //System.out.println("-x-y black explore");
            enemy = false;
            int[][] xm = pieces.get(piece).getOffset().get("-x-y");
            cross = true;
            exploreB(xm, location, 0, moves);
            cross = false;
        }
        //going to load the moves for enpassant
        if(pawn){
            String enp = loadEnPassantLocations(location, true);
            ////System.out.println("ENP IS " + enp);
            if(enpassantBlack.containsKey(enp)){
                moves.add(enp);
            }
        }

        //implement way to remove moves from moves set based on special rules
        //ex: pawns, castling, check, checkmate


        //added the new moves to the movestore

        ////System.out.println("location " + location + " empty? " + moves.isEmpty());

        black.setMoves(location, moves);
        pawn = false;

        /*
        //System.out.print("Setting: ");
        for (String loc:moves) {
            //System.out.print(loc + " ");
        }
        //System.out.println();
        */
    }

    /**
     * Method to remove a piece from MovesStore Black
     *
     * @param location
     */
    void removeBPiece(String location){
        black.remove(location);
        ////System.out.println("Deleted?" + black.contains(location));
    }

    /**
     * Method to remove a piece from MoveStore White
     *
     * @param location
     */
    void removeWPiece(String location){
        white.remove(location);
        ////System.out.println("Deleted?" + white.contains(location));
    }

    /**
     * Method to check which side is being checked
     * Deprecated since the directions require that we print check only and not the color
     *
     * @return color in check
     */
    @Deprecated
    String getCheck(){
        ////System.out.println("Black is " + checkB);
        //System.out.println("White is " + checkW);

        String check = "";
        if(checkB){
            checkB = false;
            check += " Check Black";
        }
        if(checkW){
            checkW = false;
            check += " Check White";
        }

        //System.out.println("Check is " + check);
        return check;
    }

    /**
     * Method to check which side is in checkmate
     *
     * @return String with Player in Checkmate
     */
    String getCheckMate(){
        String checkmate = "";
        if(checkMateB){
            checkmate += "Black. White Wins!";
        }
        if(checkMateW)
            checkmate += "White. Black Wins!";
        return checkmate;
    }

    /**
     * Method to get the enpassant objects by color
     *
     * @return Map of all objects in enpassant
     */
    String getEnpassant(String location){
        if(enpassant.containsKey(location)){
            String enpassloc = enpassant.get(location);

            if(black.contains(enpassloc)){
                black.remove(enpassloc);
            }
            else{
                white.remove(enpassloc);
            }

            return enpassloc;
        }
        return null;
    }

    /**
     * Method to remove a location from enpassant
     *
     * @param current
     */
    void removeEnpassant(String current){
        String location = null;
        if(enpassant.containsKey(current)){
            ////System.out.println("This piece is in enpassant curr:" + current + " loc: " + enpassant.get(current));
            location = enpassant.get(current);
            enpassant.remove(current);
            ////System.out.println("Checking remove " + enpassant.get(current));
        }
        ////System.out.println("White has it: " + enpassantWhite.containsKey(location));
        ////System.out.println("Black has it: " + enpassantBlack.containsKey(location));
        if(enpassantWhite.containsKey(location)){
            enpassantWhite.remove(location);
        }
        else{
            enpassantBlack.remove(location);
        }
    }

    /**
     * Method to reload MoveStore
     *
     * @param location
     */
    void reload(String location){
        String piece = Board.getPiece(location);
        piece = piece.trim();
        ////System.out.println("reload location: " + location + " piece " + Board.getPiece(location));
        //use the offsets for the queen
        //explore until you hit something that looks like it needs to reload

        Map<String, int[][]> offset = pieces.get("Q").getOffset();

        recurReload(offset.get("x"), location, 0);
        recurReload(offset.get("-x"), location, 0);
        recurReload(offset.get("y"), location, 0);
        recurReload(offset.get("-y"), location, 0);
        recurReload(offset.get("xy"), location, 0);
        recurReload(offset.get("x-y"), location, 0);
        recurReload(offset.get("-xy"), location, 0);
        recurReload(offset.get("-x-y"), location, 0);

        if(piece.length() == 0 || piece.compareTo("##") == 0){
            return;
        }
        if(piece.charAt(0) == 'b'){
            ////System.out.println("SELF BLACK " + location);
            setupMovesBlack(location);
        }
        else{
            ////System.out.println("SELF WHITE " + location);
            setupMovesWhite(location);
        }

        ////System.out.println("BLACK " + black.contains(location));
        ////System.out.println("WHITE " + white.contains(location));


        ////System.out.println("White Checkmate? " + checkW);
        ////System.out.println("Black Checkmate? " + checkB);

        if(checkB | checkW){
            Board.check = true;
            checkMate();
        }

    }

    /**
     * Method to set castling for a player
     *
     * @param loc
     * @param color
     */
    void setCastle(int loc, char color){
        switch (color){
            case 'b':
                occupiedBlack[loc] = false;
                break;
            case 'w':
                occupiedWhite[loc] = false;
                break;
        }
    }

    //private methods

    /**
     * Private method to load all enpassants
     */
    private void loadEnPassant(){
        //black pieces
        enpassantBlack.put("5a", false);
        enpassantBlack.put("5b", false);
        enpassantBlack.put("5c", false);
        enpassantBlack.put("5d", false);
        enpassantBlack.put("5e", false);
        enpassantBlack.put("5f", false);
        enpassantBlack.put("5g", false);
        enpassantBlack.put("5h", false);
        //white pieces
        enpassantWhite.put("4a", false);
        enpassantWhite.put("4b", false);
        enpassantWhite.put("4c", false);
        enpassantWhite.put("4d", false);
        enpassantWhite.put("4e", false);
        enpassantWhite.put("4f", false);
        enpassantWhite.put("4g", false);
        enpassantWhite.put("4h", false);
    }

    /**
     * Loads Enpassant locations based on color.
     * true = black
     * false = white
     *
     * @param location
     * @param color
     * @return location of enpassant
     */
    private String loadEnPassantLocations(String location, boolean color){
        int yo;
        if(color){
            yo = -2;
        }
        else
            yo = 2;

        //calculate the new positions
        char yc = location.charAt(0);
        yc += yo;
        char xc = location.charAt(1);

        //getting new location and preparing for checks
        String nlocation = yc+ "" + xc;
        return nlocation;
    }

    /**
     * Private method handles the exploration of the board for legal moves for white pieces.
     *
     * @param offset
     * @param location
     * @param count
     * @param moves
     */
    private void exploreW(int[][] offset, String location, int count, Set<String> moves){
        //if(pawn)
        //    //System.out.println("location: " + location + " count: " + count + " length: " + offset.length);
        if(count >= offset.length)
            return;
        int[] arr = offset[count];
        ////System.out.println(location + " " + arr[0] + " " + arr[1]);

        int xo = arr[0];
        int yo = arr[1];

        //calculate the new positions
        char yc = location.charAt(0);
        yc += yo;
        char xc = location.charAt(1);
        xc += xo;

        //getting new location and preparing for checks
        String nlocation = yc+ "" + xc;
        xo = xc;
        yo = yc;

        ////System.out.println(x + " " + y);
        ////System.out.println("New Location: " + nlocation);

        //boundary check
        if(yo < 49 | yo > 56 | xo < 97 | xo > 104){
            ////System.out.println("Problem X or Y");
            return;
        }

        //color match check
        String locPiece = Board.getPiece(nlocation);
        if(locPiece.length() > 0 && locPiece.charAt(0) == 'w'){
            ////System.out.println("Problem Color");
            return;
        }

        //the previous location has an enemy and you cannot just jump over enemies
        if(enemy){
            enemy = false;
            return;
        }

        String enemyPiece = Board.getPiece(nlocation).trim();

        if(enemyPiece.length() > 0 && enemyPiece.compareTo("##") != 0){
            this.enemy = true;
            if(pawn && forward){
                return;
            }
        }
        ////System.out.println("Black Enemy " + enemy);
        ////System.out.println("Pawn" + pawn);

        //making sure pawns cannot go across unless it is occupied
        if(pawn){
            if(cross){
                //check if enpass location is filled
                yc--;
                String enp = yc+ "" + xc;
                String piece = Board.getPiece(enp);
                ////System.out.println("Enpassant location expB " + location + " nloc " + nlocation + " enp " + enp + " " + piece);
                if(enpassantBlack.containsKey(enp)){
                    if(enpassantBlack.get(enp)){
                        //check if the location in front is filled
                        char xpc = location.charAt(1);
                        char ypc = location.charAt(0);
                        ypc++;
                        String check = ypc+ "" + xpc;
                        ////System.out.println("Check location " + check + " check if black has it " + black.contains(check));
                        if(!black.contains(check)){
                            //add and return
                            moves.add(nlocation);
                            enpassant.put(nlocation, enp);
                            return;
                        }
                    }
                }
                //there is no enemy there
                if(!enemy) {
                    return;
                }
            }
        }

        if(nlocation.compareTo(blackKing) == 0){
            //System.out.println("CHECK Black");
            checkB = true;
        }

        moves.add(nlocation);
        count++;
        exploreW(offset, location, count, moves);
    }

    /**
     * Private method handles the exploration of the board for legal moves for black pieces.
     *
     * @param offset
     * @param location
     * @param count
     * @param moves
     */
    private void exploreB(int[][] offset, String location, int count, Set<String> moves){
        if(count >= offset.length)
            return;
        int[] arr = offset[count];

        if(pawn){
            ////System.out.println("location: " + location + " count: " + count + " length: " + offset.length);
            ////System.out.println(location + " " + arr[0] + " " + arr[1]);
        }

        int xo = arr[0];
        int yo = arr[1];

        //calculate the new positions
        char yc = location.charAt(0);
        yc += yo;
        char xc = location.charAt(1);
        xc += xo;

        //getting new location and preparing for checks
        String nlocation = yc+ "" + xc;
        xo = xc;
        yo = yc;

        ////System.out.println(x + " " + y);
        ////System.out.println("New Location: " + nlocation);

        //boundary check
        if(yo < 49 | yo > 56 | xo < 97 | xo > 104){
            ////System.out.println("Problem X or Y");
            return;
        }

        ////System.out.println(0);

        //color match check
        String locPiece = Board.getPiece(nlocation);
        if(locPiece.length() > 0 && locPiece.charAt(0) == 'b'){
            ////System.out.println("Problem Color");
            return;
        }

        ////System.out.println(1);

        //the previous location has an enemy and you cannot just jump over enemies
        if(enemy){
            enemy = false;
            return;
        }

        ////System.out.println(2);
        String enemyPiece = Board.getPiece(nlocation).trim();


        ////System.out.println("Black Enemy " + enemy);

        if(enemyPiece.length() > 0 && enemyPiece.compareTo("##") != 0){
            ////System.out.println("AN ENEMY");
            this.enemy = true;
            if(pawn && forward){
                return;
            }
        }
        ////System.out.println("Pawn" + pawn);
        ////System.out.println(3);

        //making sure pawns cannot go across unless it is occupied
        if(pawn){
            if(cross){
                //check if enpass location is filled
                yc++;
                String enp = yc+ "" + xc;
                String piece = Board.getPiece(enp);
                ////System.out.println("Enpassant location expB " + location + " nloc " + nlocation + " enp " + enp + " " + piece);
                if(enpassantWhite.containsKey(enp)){
                    if(enpassantWhite.get(enp)){
                        //check if the location in front is filled
                        char xpc = location.charAt(1);
                        char ypc = location.charAt(0);
                        ypc--;
                        String check = ypc+ "" + xpc;
                        ////System.out.println("Check location " + check + " check if black has it " + black.contains(check));
                        if(!black.contains(check)){
                            //add and return
                            moves.add(nlocation);
                            enpassant.put(nlocation, enp);
                            return;
                        }
                    }
                }
                //there is no enemy there
                if(!enemy) {
                    return;
                }
            }
            ////System.out.println(5);
        }


        if(nlocation.compareTo(whiteKing) == 0){
            //System.out.println("CHECK White");
            checkB = true;
        }

        if(nlocation.compareTo(blackKing) == 0){
            //System.out.println("CHECK Black");
            checkB = true;
        }

        ////System.out.println("RECUR START");

        moves.add(nlocation);
        count++;
        exploreB(offset, location, count, moves);
    }

    /**
     * Method to recursively explore from a given position for pieces to reload
     *
     * @param offset
     * @param location
     * @param count
     */
    private void recurReload(int[][] offset, String location, int count){
        if(count >= offset.length)
            return;
        int[] arr = offset[count];
        ////System.out.println(location + " " + arr[0] + " " + arr[1]);

        int xo = arr[0];
        int yo = arr[1];

        //calculate the new positions
        char yc = location.charAt(0);
        yc += yo;
        char xc = location.charAt(1);
        xc += xo;

        //getting new location and preparing for checks
        String nlocation = yc+ "" + xc;
        xo = xc;
        yo = yc;

        ////System.out.println(x + " " + y);
        ////System.out.println("New Location: " + nlocation);

        //boundary check
        if(yo < 49 | yo > 56 | xo < 97 | xo > 104){
            ////System.out.println("Problem X or Y");
            return;
        }

        String piece = Board.getPiece(nlocation);
        piece = piece.trim();

        if(piece.length() > 0 && piece.compareTo("##") != 0){
            ////System.out.println("Reload this: " + piece + " at " + nlocation);
            if(piece.charAt(0) == 'b'){
                ////System.out.println("Reload Black");
                setupMovesBlack(nlocation);
            }
            else{
                //////System.out.println("Reload White");
                setupMovesWhite(nlocation);
            }



            return;
        }
        count++;
        recurReload(offset, location, count);
    }

    /**
     * Private method checks on checkmate situation
     */
    private void checkMate(){
        if(checkW){
            setupMovesWhite(whiteKing);
            int count = white.moveCount(whiteKing);
            if(count == 0){
                checkMateW = true;
                Board.checkmate = true;
            }
            if(count == 1){
                Object[] moves = white.getMove(whiteKing);
                ////System.out.println("White King Moves");
                String piece = Board.getPiece((String)moves[0]);
                ////System.out.println("Piece is " + piece);
                if(piece.charAt(0) == 'b'){
                    checkMateW = false;
                }
                else{
                    checkMateW = true;
                }
            }
        }
        if(checkB){
            setupMovesBlack(blackKing);
            int count = black.moveCount(blackKing);
            if(count == 0){
                checkMateB = true;
                Board.checkmate = true;
            }
            if(count == 1){
                Object[] moves = black.getMove(blackKing);
                //System.out.println("Black King Moves");
                String piece = Board.getPiece((String)moves[0]);
                //System.out.println("Piece is " + piece);
                if(piece.charAt(0) == 'w'){
                    checkMateB = false;
                }
                else{
                    checkMateB = true;
                }
            }
        }
    }
}

