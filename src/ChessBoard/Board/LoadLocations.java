package ChessBoard.Board;

import java.util.HashMap;
import java.util.Map;

/**
 * Class used to initially load the MoveStores
 *
 * @author Jaya Kasa
 * @version 1.0
 */
@Deprecated
class LoadLocations {
    LoadLocations(){
    }

    /**
     * Loads the Black Pieces
     *
     * @return Map of black pieces
     */
    Map<String, Locations> loadBlackLocations(){
        Map<String, Locations> locationsMap = new HashMap<>();

        locationsMap.put("8a", new Locations("R"));
        locationsMap.put("8b", new Locations("N"));
        locationsMap.put("8c", new Locations("B"));
        locationsMap.put("8d", new Locations("Q"));
        locationsMap.put("8e", new Locations("K"));
        locationsMap.put("8f", new Locations("B"));
        locationsMap.put("8g", new Locations("N"));
        locationsMap.put("8h", new Locations("R"));

        locationsMap.put("7a", new Locations("p"));
        locationsMap.put("7b", new Locations("p"));
        locationsMap.put("7c", new Locations("p"));
        locationsMap.put("7d", new Locations("p"));
        locationsMap.put("7e", new Locations("p"));
        locationsMap.put("7f", new Locations("p"));
        locationsMap.put("7g", new Locations("p"));
        locationsMap.put("7h", new Locations("p"));

        return locationsMap;
    }

    /**
     * Loads the White Pieces
     *
     * @return Map of white pieces
     */
    Map<String, Locations> loadWhiteLocations(){
        Map<String, Locations> locationsMap = new HashMap<>();

        locationsMap.put("1a", new Locations("R"));
        locationsMap.put("1b", new Locations("N"));
        locationsMap.put("1c", new Locations("B"));
        locationsMap.put("1d", new Locations("Q"));
        locationsMap.put("1e", new Locations("K"));
        locationsMap.put("1f", new Locations("B"));
        locationsMap.put("1g", new Locations("N"));
        locationsMap.put("1h", new Locations("R"));

        locationsMap.put("2a", new Locations("p"));
        locationsMap.put("2b", new Locations("p"));
        locationsMap.put("2c", new Locations("p"));
        locationsMap.put("2d", new Locations("p"));
        locationsMap.put("2e", new Locations("p"));
        locationsMap.put("2f", new Locations("p"));
        locationsMap.put("2g", new Locations("p"));
        locationsMap.put("2h", new Locations("p"));

        return locationsMap;
    }
}
