package ChessBoard.Board;

import java.util.*;

/**
 * Class that stores the LegalMoves of a certain piece
 * Class only handles the storage
 *
 * @author Jaya Kasa
 * @version 1.0
 */
final class MoveStore {

    private char color;
    //string stores the location of the king at a given moment
    private String kingLocBlack;
    private String kingLocWhite;
    //key: location
    //value: moves
    private Map<String, Set<String>> moves;
    private List<String> locations = null;

    /**
     * Private Constructor to prevent more than 2 objects from ever existing
     *
     * @param color
     */
    private MoveStore(char color){
        this.color = color;
        moves = new HashMap<>();
        locations = new LinkedList<>();
    }

    /**
     * Method gets the MoveStore object for Player Black.
     *
     * @return MoveStore object for Player Black
     */
    static MoveStore getBlackMoveStore(){
        return new MoveStore('b');
    }

    /**
     * Method gets the MoveStore object for Player White.
     *
     * @return MoveStore object for Player White
     */
    static MoveStore getWhiteMoveStore(){
        return new MoveStore('w');
    }

    /**
     * Method to set the moves for a specific piece.
     *
     * @param location
     * @param set
     */
    void setMoves(String location, Set<String> set){
        moves.put(location, set);
        locations.add(location);
    }

    /**
     * Method to remove a set of moves for a specific piece.
     *
     * @param location
     */
    void remove(String location){
        if(moves.containsKey(location)){
            moves.remove(location);
        }
    }

    /**
     * Method for checking if a specific piece has a given move.
     *
     * @param start
     * @param finish
     * @return true if move is allowed, else false
     */
    boolean hasMove(String start, String finish){
        //System.out.println("MoveStore hasMove: " + start + " " + finish);
        if(!moves.containsKey(start)){
            return false;
        }
        return moves.get(start).contains(finish);
    }

    /**
     * Method to check if a certain piece exists in the Movestore.
     *
     * @param location
     * @return true if piece at location exists, else false.
     */
    boolean contains(String location){
        return moves.containsKey(location);
    }

    /**
     * Method for printing all possible moves at given location.
     * For use in debugging.
     *
     * @param location
     */
    void printMoves(String location){
        System.out.print("Moves: ");
        Set<String> move= moves.get(location);
        if(move == null){
            System.out.println("NULL ISSUE");
            return;
        }
        for(String s : move){
            System.out.print(s + " ");
        }
        System.out.println();
    }

    /**
     * Method for adding a move to a given piece at a given location.
     *
     * @param location
     * @param move
     */
    void addMove(String location, String move){
        moves.get(location).add(move);
    }

    /**
     * Method for getting the number of possible moves for a specific piece.
     *
     * @param location
     * @return number of moves for a given piece at given location
     */
    int moveCount(String location){
        return moves.get(location).size();
    }

    /**
     * Method for getting an array of all moves for a given location.
     *
     * @param location
     * @return Object array containing all moves for a given piece at specified location.
     */
    Object[] getMove(String location){
        return moves.get(location).toArray();
    }

}
