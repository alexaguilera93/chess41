package ChessBoard.Board;

import java.util.HashSet;
import java.util.Set;

/**
 * Class defines a way to store the locations of a certain type of piece.
 *
 * @author Jaya Kasa
 * @version 1.0
 */
@Deprecated
class Locations {

    //piece type
    private String piece;
    //set of all possible locations for given piece
    private Set<String> locations;

    Locations(String piece){
        this.piece = piece;
        locations = new HashSet<>();

    }

    /**
     * Method to check if location is legal
     *
     * @param location
     * @return true if location exists, else false
     */
    boolean hasLocation(String location){
        return locations.contains(location);
    }

    /**
     * Method to set the locations
     *
     * @param locations
     */
    void setLocations(Set<String> locations){
        this.locations = locations;
    }

    String getPiece(){
        return piece;
    }
}
