package ChessBoard.Board;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

/**
 * Class loads and prints the board that is displayed.
 * Class does not handle the legality of a move.
 *
 * @author Jaya Kasa
 * @version 1.5
 */
public final class Board {

    //instance vars
    private static Map<String, String> board = new TreeMap<>();
    private static final Map<String, String> space = new HashMap<>();
    private static Board boardObj;
    private Legal legal;
    static boolean check;
    static boolean checkmate;
    private static char promo = 'Q';

    /**
     * Private Constructor to prevent initialization
     */
    private Board(){
        legal = Legal.getInstance();
        loadBoard();
    }

    /**
     * Static method to get instance of the board
     *
     * @return Board object
     */
    public static Board getInstance(){
        if(boardObj == null){
            boardObj = new Board();
        }
        return boardObj;
    }

    /**
     * Method to set a piece in a specific position on the board
     * O(1)
     *
     * @param start
     * @param finish
     */
    public String setPiece(String start, String finish, char colorIncoming){
        //incorrect input coordinates
        if(!board.containsKey(start) | !board.containsKey(finish)){
            return "false";
        }
        if(board.get(start).compareTo("  ") == 0 | board.get(start).compareToIgnoreCase("##") == 0){
            return "false";
        }
        String piece = board.get(start);
        //System.out.println("Board setPiece: " + piece);
        boolean isLegal = legal.isLegal(colorIncoming, start, finish);

        //System.out.println("Legal came back: " + isLegal);
        if(isLegal){

        	//System.out.println(space.get(start));
        	board.put(start,space.get(start));
            String removed = board.get(finish);
            //System.out.println("AT START " + board.get(start) + " TO BE REMOVED " + removed);
            board.remove(finish);
        	board.put(finish,piece);
        	if(finish.charAt(0) == '8'){
        		if(piece.equals("wp")){
            			board.remove(finish);

                            //scanner.close();
                            String np = "w" + promo;
                            //System.out.println("np is " + np);
                            //System.out.println(np);
                            board.put(finish, np);
                            promo = 'Q';                      
        		}
        	}
        	if(finish.charAt(0) =='1'){
        		if(piece.equals("bp")){
        			board.remove(finish);

                        //scanner.close();
                        String np = "b" + promo;
                        //System.out.println(np);
                        board.put(finish, np);
                        promo = 'Q';
        		}
        	}

            if(removed.charAt(0) == 'b'){
                legal.removeBPiece(finish);
            }
            if(removed.charAt(0) == 'w'){
                legal.removeWPiece(finish);
            }

            String enpassant = legal.getEnpassant(finish);
            if(enpassant != null){
                board.remove(enpassant);
                board.put(enpassant, space.get(enpassant));
                legal.removeEnpassant(finish);
            }

            // TODO this will need a better implementation at some point because this is the biggest drag on runtime
            //reload the moves
            //consider threads here or a better algorithm because this will be O(nlogn)
            legal.reload(start);
            legal.reload(finish);

            if(enpassant != null){
                legal.reload(enpassant);
            }

            if(checkmate){
                //System.out.println("CHECKMATE");
                checkmate = false;
                return "Checkmate " + legal.getCheckMate();
            }
            if(check){
                //System.out.println("CHECK");
                check = false;
                return "Check";
            }

        	return "true";
        }
        return "false";
    }

    /**
     * Method that loads the board that is eventually drawn
     *
     * O(1)
     */
    private void loadBoard(){

        //setting up the board
        board.put("1a", "wR");
        board.put("1b", "wN");
        board.put("1c", "wB");
        board.put("1d", "wQ");
        board.put("1e", "wK");
        board.put("1f", "wB");
        board.put("1g", "wN");
        board.put("1h", "wR");

        board.put("2a", "wp");
        board.put("2b", "wp");
        board.put("2c", "wp");
        board.put("2d", "wp");
        board.put("2e", "wp");
        board.put("2f", "wp");
        board.put("2g", "wp");
        board.put("2h", "wp");

        board.put("3a", "##");
        board.put("3b", "  ");
        board.put("3c", "##");
        board.put("3d", "  ");
        board.put("3e", "##");
        board.put("3f", "  ");
        board.put("3g", "##");
        board.put("3h", "  ");

        board.put("4a", "  ");
        board.put("4b", "##");
        board.put("4c", "  ");
        board.put("4d", "##");
        board.put("4e", "  ");
        board.put("4f", "##");
        board.put("4g", "  ");
        board.put("4h", "##");

        board.put("5a", "##");
        board.put("5b", "  ");
        board.put("5c", "##");
        board.put("5d", "  ");
        board.put("5e", "##");
        board.put("5f", "  ");
        board.put("5g", "##");
        board.put("5h", "  ");

        board.put("6a", "  ");
        board.put("6b", "##");
        board.put("6c", "  ");
        board.put("6d", "##");
        board.put("6e", "  ");
        board.put("6f", "##");
        board.put("6g", "  ");
        board.put("6h", "##");

        board.put("7a", "bp");
        board.put("7b", "bp");
        board.put("7c", "bp");
        board.put("7d", "bp");
        board.put("7e", "bp");
        board.put("7f", "bp");
        board.put("7g", "bp");
        board.put("7h", "bp");

        board.put("8a", "bR");
        board.put("8b", "bN");
        board.put("8c", "bB");
        board.put("8d", "bQ");
        board.put("8e", "bK");
        board.put("8f", "bB");
        board.put("8g", "bN");
        board.put("8h", "bR");

        //possible location of future bug
        legal.setupMovesWhite("2a");
        legal.setupMovesWhite("2b");
        legal.setupMovesWhite("2c");
        legal.setupMovesWhite("2d");
        legal.setupMovesWhite("2e");
        legal.setupMovesWhite("2f");
        legal.setupMovesWhite("2g");
        legal.setupMovesWhite("2h");

        legal.setupMovesWhite("1a");
        legal.setupMovesWhite("1b");
        legal.setupMovesWhite("1c");
        legal.setupMovesWhite("1d");
        legal.setupMovesWhite("1e");
        legal.setupMovesWhite("1f");
        legal.setupMovesWhite("1g");
        legal.setupMovesWhite("1h");

        legal.setupMovesBlack("7a");
        legal.setupMovesBlack("7b");
        legal.setupMovesBlack("7c");
        legal.setupMovesBlack("7d");
        legal.setupMovesBlack("7e");
        legal.setupMovesBlack("7f");
        legal.setupMovesBlack("7g");
        legal.setupMovesBlack("7h");

        legal.setupMovesBlack("8a");
        legal.setupMovesBlack("8b");
        legal.setupMovesBlack("8c");
        legal.setupMovesBlack("8d");
        legal.setupMovesBlack("8e");
        legal.setupMovesBlack("8f");
        legal.setupMovesBlack("8g");
        legal.setupMovesBlack("8h");

        // saving spaces
        space.put("1a", "##");
        space.put("1b", "  ");
        space.put("1c", "##");
        space.put("1d", "  ");
        space.put("1e", "##");
        space.put("1f", "  ");
        space.put("1g", "##");
        space.put("1h", "  ");

        space.put("2a", "  ");
        space.put("2b", "##");
        space.put("2c", "  ");
        space.put("2d", "##");
        space.put("2e", "  ");
        space.put("2f", "##");
        space.put("2g", "  ");
        space.put("2h", "##");

        space.put("3a", "##");
        space.put("3b", "  ");
        space.put("3c", "##");
        space.put("3d", "  ");
        space.put("3e", "##");
        space.put("3f", "  ");
        space.put("3g", "##");
        space.put("3h", "  ");

        space.put("4a", "  ");
        space.put("4b", "##");
        space.put("4c", "  ");
        space.put("4d", "##");
        space.put("4e", "  ");
        space.put("4f", "##");
        space.put("4g", "  ");
        space.put("4h", "##");

        space.put("5a", "##");
        space.put("5b", "  ");
        space.put("5c", "##");
        space.put("5d", "  ");
        space.put("5e", "##");
        space.put("5f", "  ");
        space.put("5g", "##");
        space.put("5h", "  ");

        space.put("6a", "  ");
        space.put("6b", "##");
        space.put("6c", "  ");
        space.put("6d", "##");
        space.put("6e", "  ");
        space.put("6f", "##");
        space.put("6g", "  ");
        space.put("6h", "##");

        space.put("7a", "##");
        space.put("7b", "  ");
        space.put("7c", "##");
        space.put("7d", "  ");
        space.put("7e", "##");
        space.put("7f", "  ");
        space.put("7g", "##");
        space.put("7h", "  ");

        space.put("8a", "  ");
        space.put("8b", "##");
        space.put("8c", "  ");
        space.put("8d", "##");
        space.put("8e", "  ");
        space.put("8f", "##");
        space.put("8g", "  ");
        space.put("8h", "##");
    }

    /**
     * Method to retrieve a piece at a given location
     *
     * @param location
     * @return String representation of piece at location
     */
    static String getPiece(String location){
        return board.get(location);
    }

    /**
     * Method to get the board
     *
     * @return Map object representing the board
     */
    static Map<String, String> getBoard(){
        return board;
    }

    /**
     * Static method to aid in promotion of a piece
     *
     * @param location
     * @param piece
     */
    static void promotePiece(String location, String piece){
        board.put(location, piece);
    }

    //O(n)
    @Override
    public String toString(){
        Set<String> keys = board.keySet();
        String printBoard = "", line = "";

        int loc = 1;
        int count = 1;
        for(String key : keys){
            String piece = board.get(key);
            //System.out.println(key + " " + piece);
            line += piece;

            if(count%8 == 0) {
                printBoard = line + " " + loc + "\n" + printBoard;
                //System.out.println(printBoard);
                line = "";
                loc++;
            }
            else
                line += " ";

            count++;
        }
        printBoard += " a  b  c  d  e  f  g  h";
        return printBoard;
    }
    
    /**
     * Take in what to promote piece to
     *
     * @param promo piece to promote up to
     */
    public void setPromo(char promo){
    	this.promo = promo;
    }
}
