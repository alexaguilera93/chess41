package ChessBoard.Exception;

/**
 * Exception thrown when a checkmate occurs
 *
 * @author Jaya Kasa
 * @version 1.0
 */
@Deprecated
public class CheckMate extends Exception{
    /**
     * Constructor for a Checkmate Exception.
     * Message defines the player that received the checkmate
     *
     * @param message
     */
    public CheckMate(String message){
        super(message);
    }
}
