package ChessBoard.Exception;

/**
 * Exception thrown when a Check occurs
 *
 * @author Jaya Kasa
 * @version 1.0
 */
@Deprecated
public class Check extends Exception{
    public Check(String message){
        super(message);
    }
}
