Piece --> Abstract Class
King, Queen, Rook, Pawn, Knight, Bishop --> Final Subclasses of Piece

Pieces store all possible locations that a piece can move to from any position on the board

Board draws and maintains the chess board

Legal handles the majority of the game play logic and mechanics.

MoveStore stores all possible moves in order to enable quick look ups.

ChessBoard package can act as a standalone package.
All one needs to do to is use the Chessboard class and they can create any front end implementation they like.

Classes Location and LoadLocation are Deprecated and no longer in use in this program.